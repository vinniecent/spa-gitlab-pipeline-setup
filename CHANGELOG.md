# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.8 (2020-03-23)


### Features

* master release ([01960f5](https://gitlab.com/vinniecent/spa-gitlab-pipeline-setup/commit/01960f5631d0c55ec3e6bb262ca74291f9a25c65))
* setup gitlab ci pipeline for SPA ([760be5e](https://gitlab.com/vinniecent/spa-gitlab-pipeline-setup/commit/760be5eeba6a32ca18d1f5e9bb92dd686203e2b4))
